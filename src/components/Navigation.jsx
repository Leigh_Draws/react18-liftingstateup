import "./Navigation.css";

function Navigation(props) {
  // TODO get user from props drilling
  // On récupère la variable user déclarée dans App et on dit qu'on va s'en servir comme props
const { user } = props;

  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/* TODO check if user is connected */}
      {/* On utilise la props dans un opérateur ternaire afin de voir si l'utilisateur est connecté ou non */}
      <li>{ user ? "Hello, " + user.name + " !" : "Please log in"}</li>
      
    
    </ul>
  );
}

export default Navigation;
