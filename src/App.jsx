import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {

  // Ici on déclare le state pour pouvoir l'utiliser à d'autres endroits après
  const [user, setUser] = useState(null);

  return (
    <div className="App">

    {/* Au moment d'appeler le composant on déclare la variable user qui équivaut au state user */}
      <Navigation user={user}/>
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

export default App;
